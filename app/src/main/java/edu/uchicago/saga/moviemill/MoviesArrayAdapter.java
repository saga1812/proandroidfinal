package edu.uchicago.saga.moviemill;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MoviesArrayAdapter extends ArrayAdapter<Movie>{
    // class for reusing views as list items scroll off and onto the screen
    private static class ViewHolder {
        ImageView posterImageView;
        TextView titleTextView;
        TextView synopsisTextView;
        TextView voteTextView;
        TextView genreTextView;
        TextView releaseTextView;
    }

    // stores already downloaded Bitmaps for reuse
    private Map<String, Bitmap> bitmaps = new HashMap<>();

    // constructor to initialize superclass inherited members
    public MoviesArrayAdapter(Context context, List<Movie> movieDetails) {
        super(context, -1, movieDetails);
    }
    // creates the custom views for the ListView's items
    @SuppressLint("StringFormatMatches")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // get Movie object for this specified ListView position
        Movie movieItem = getItem(position);

        ViewHolder viewHolder; // object that reference's list item's views

        // check for reusable ViewHolder from a ListView item that scrolled
        // offscreen; otherwise, create a new ViewHolder
        if (convertView == null) { // no reusable ViewHolder, so create one
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView =
                    inflater.inflate(R.layout.list_item, parent, false);
            viewHolder.posterImageView =
                    (ImageView) convertView.findViewById(R.id.posterImageView);
            viewHolder.titleTextView =
                    (TextView) convertView.findViewById(R.id.titleTextView);
            viewHolder.synopsisTextView =
                    (TextView) convertView.findViewById(R.id.synopsisTextView);
            viewHolder.voteTextView =
                    (TextView) convertView.findViewById(R.id.voteTextView);
            viewHolder.genreTextView =
                    (TextView) convertView.findViewById(R.id.genreTextView);
            viewHolder.releaseTextView =
                    (TextView) convertView.findViewById(R.id.releaseTextView);
            convertView.setTag(viewHolder);
        }
        else { // reuse existing ViewHolder stored as the list item's tag
            viewHolder = (ViewHolder) convertView.getTag();
        }



        // get other data from Movie object and place into views
        Context context = getContext(); // for loading String resources
        viewHolder.titleTextView.setText(context.getString(
                R.string.movie_title, movieItem.title));
        viewHolder.synopsisTextView.setText(
                context.getString(R.string.Synopsis, movieItem.synopsis));
        viewHolder.voteTextView.setText(
                context.getString(R.string.vote_avg, movieItem.voteAvg));
        viewHolder.genreTextView.setText(
                context.getString(R.string.genres, movieItem.genreIDs));
        viewHolder.releaseTextView.setText(
                context.getString(R.string.release_date, movieItem.releaseDate));

        Glide.with(getContext())
                .load(movieItem.posterURL)
//                 .placeholder(R.color.photo_placeholder)
//                 .error(R.color.photo_placeholder_error)
                .into(viewHolder.posterImageView);



        //http://stackoverflow.com/questions/5324004/how-to-display-static-google-map-on-android-imageview


        return convertView; // return completed list item to display
    }


}

