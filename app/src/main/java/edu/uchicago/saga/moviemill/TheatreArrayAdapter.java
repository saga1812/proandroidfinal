package edu.uchicago.saga.moviemill;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import com.bumptech.glide.Glide;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TheatreArrayAdapter extends  ArrayAdapter<Theatre>{

    // class for reusing views as list items scroll off and onto the screen
    private static class ViewHolder {
        ImageView iconImageView;
        TextView theatreTextView;
        TextView openTextView;
        TextView ratingTextView;
        TextView distanceTextView;
    }

    // stores already downloaded Bitmaps for reuse
    private Map<String, Bitmap> bitmaps = new HashMap<>();

    // constructor to initialize superclass inherited members
    public TheatreArrayAdapter(Context context, List<Theatre> forecast) {
        super(context, -1, forecast);
    }

    // creates the custom views for the ListView's items
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // get Weather object for this specified ListView position
        Theatre theatre = getItem(position);

        ViewHolder viewHolder; // object that reference's list item's views

        // check for reusable ViewHolder from a ListView item that scrolled
        // offscreen; otherwise, create a new ViewHolder
        if (convertView == null) { // no reusable ViewHolder, so create one
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView =
                    inflater.inflate(R.layout.theatre_list_item, parent, false);
            viewHolder.iconImageView =
                    (ImageView) convertView.findViewById(R.id.iconImageView);
            viewHolder.ratingTextView =
                    (TextView) convertView.findViewById(R.id.ratingTextView);
            viewHolder.openTextView =
                    (TextView) convertView.findViewById(R.id.openTextView);
            viewHolder.theatreTextView =
                    (TextView) convertView.findViewById(R.id.theatreTextView);
            viewHolder.distanceTextView =
                    (TextView) convertView.findViewById(R.id.distanceTextView);

            convertView.setTag(viewHolder);
        }
        else { // reuse existing ViewHolder stored as the list item's tag
            viewHolder = (ViewHolder) convertView.getTag();
        }



        // get other data from Weather object and place into views
        Context context = getContext(); // for loading String resources
        viewHolder.theatreTextView.setText(context.getString(
                R.string.theatre_name, theatre.getTheatreName()));
        viewHolder.openTextView.setText(
                context.getString(R.string.open_now, theatre.getOpenNow()));
        viewHolder.ratingTextView.setText(
                context.getString(R.string.ratings, theatre.getRating()));
        viewHolder.distanceTextView.setText(
                context.getString(R.string.distance, theatre.getDistance()));

        Glide.with(getContext())
                .load(theatre.geticonURL())
//                 .placeholder(R.color.photo_placeholder)
//                 .error(R.color.photo_placeholder_error)
                .into(viewHolder.iconImageView);



        //http://stackoverflow.com/questions/5324004/how-to-display-static-google-map-on-android-imageview


        return convertView; // return completed list item to display
    }


}

/**************************************************************************
 * (C) Copyright 1992-2016 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 **************************************************************************/
