package edu.uchicago.saga.moviemill;

import java.text.NumberFormat;

public class Theatre {
    public final String theatreName;
    public final String rating;
    public final String openNow;
    public final String iconURL;
    public final String latitude;
    public final String longitude;
    public final String distance;

    public Theatre(String theatreName, String rating, String openNow, String latitude,
                   String longitude,String distance, String iconURL){
        NumberFormat numberFormat = NumberFormat.getInstance();
        this.theatreName = theatreName;
        this.rating = rating;
        this.openNow = openNow;
        this.iconURL =  iconURL;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
    }

    public String getTheatreName() {
        return this.theatreName;
    }
    public String getRating() {
        return this.rating;
    }
    public  String getOpenNow() {return this.openNow;}
    public String geticonURL() { return this.iconURL; }
    public String getLatitude(){return this.latitude;}
    public String getLongitude() {return this.longitude;}
    public String getDistance() {return this.distance;}
}
