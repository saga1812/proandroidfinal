package edu.uchicago.saga.moviemill;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MoviesDbAdapter {
    //these are the column names
    public static final String COL_MOVIEID = "_id";
    public static final String COL_RELEASE_DATE = "release";
    public static final String COL_VOTE_AVG = "votes";
    public static final String COL_GENREID = "genres";
    public static final String COL_SYNOPSIS = "synopsis";
    public static final String COL_TITLE = "title";
    public static final String COL_POSTERURL = "image";


    //used for logging
    private static final String TAG = "MoviesDbAdapter";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;
    private static final String DATABASE_NAME = "dba_movies";
    private static final String TABLE_NAME = "tbl_movies";
    private static final int DATABASE_VERSION = 1;
    private final Context mCtx;
    //SQL statement used to create the database
    private static final String DATABASE_CREATE =
            "CREATE TABLE if not exists " + TABLE_NAME + " ( " +
                    COL_MOVIEID + " TEXT PRIMARY KEY," +
                    COL_RELEASE_DATE + " TEXT, " +
                    COL_VOTE_AVG + " TEXT, " +
                    COL_GENREID + " TEXT, " +
                    COL_SYNOPSIS + " TEXT, "+
                    COL_TITLE + " TEXT, " +
                    COL_POSTERURL + " TEXT );"
            ;


    public MoviesDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }
    //open
    public void open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
    }
    //close
    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    //CREATE
    public void createMovie(String movieID, String release, String vote, String genre,
                            String synopsis, String title, String posterurl) {
        ContentValues values = new ContentValues();
        values.put(COL_MOVIEID, movieID);
        values.put(COL_RELEASE_DATE, release);
        values.put(COL_VOTE_AVG, vote);
        values.put(COL_GENREID, genre);
        values.put(COL_SYNOPSIS, synopsis);
        values.put(COL_TITLE, title);
        values.put(COL_POSTERURL, posterurl);

        mDb.insert(TABLE_NAME, null, values);
    }


    public Cursor fetchAllMovies() {
        Cursor mCursor = mDb.query(TABLE_NAME, new String[]{COL_MOVIEID, COL_RELEASE_DATE,
                COL_VOTE_AVG, COL_GENREID, COL_SYNOPSIS, COL_TITLE, COL_POSTERURL },
                null, null, null, null, null
        );
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }
    //DELETE
    public void deleteMovieById(String nId) {
        mDb.delete(TABLE_NAME, COL_MOVIEID + "=?", new String[]{String.valueOf(nId)});
    }
    public void deleteAllMovies() {
        mDb.delete(TABLE_NAME, null, null);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.w(TAG, DATABASE_CREATE);
            db.execSQL(DATABASE_CREATE);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }
}

