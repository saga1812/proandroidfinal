// Referenced from https://bitbucket.org/csgerber/labweatherredux

package edu.uchicago.saga.moviemill;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import edu.uchicago.saga.moviemill.dummy.DummyContent;

public class MainActivity extends AppCompatActivity implements
        MovieFragment.OnListFragmentInteractionListener,
        FavFragment.OnFragmentInteractionListener,
        ShowsFragment.OnFragmentInteractionListener
{
    public static final String FRAG_ORDER_KEY = "FRAG_ORDER_KEY";
    private Map map;
    private int key = R.id.navigation_home;

    private MovieFragment fragHome;
    private FavFragment fragFav;
    private ShowsFragment fragShow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        fragFav = FavFragment.newInstance("Favorite movies here", "");
        fragHome = MovieFragment.newInstance(1);
        fragShow = ShowsFragment.newInstance("Movies now playing in theatres", "");

        map = new HashMap();
        map.put(R.id.navigation_home, fragHome);
        map.put(R.id.navigation_dashboard, fragFav);
        map.put(R.id.navigation_notification, fragShow);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                setFragFromMenu(item.getItemId());
                return true;
            }

        });


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(FRAG_ORDER_KEY, key);
        super.onSaveInstanceState(outState);

    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {


        key = savedInstanceState.getInt(FRAG_ORDER_KEY, R.id.navigation_home);
        setFragFromMenu(key);

        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onResume() {
        setFragFromMenu(key);

        super.onResume();
    }

    private void setFragFromMenu(int id) {
        key = id;
        swapInFragment((Fragment)map.get(id), R.id.container);


    }

    private void swapInFragment(Fragment fragment, int containerId){
        FragmentTransaction t = getSupportFragmentManager()
                .beginTransaction();

        t.replace(containerId, fragment);
        t.commit();
    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {
        Toast.makeText(this, item.content, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFragmentInteraction(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
