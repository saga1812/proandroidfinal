package edu.uchicago.saga.moviemill;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import edu.uchicago.saga.moviemill.dummy.DummyContent;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FavFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FavFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

// IMPORTANT NOTE
// This fragment allows you to add any movie you have searched to your favorites lists.
// You can also click on any item and delete it by clicking on the delete item dialog
// from the favorites list

public class FavFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private MoviesArrayAdapter movieArrayAdapter;
    private ListView moviesListView;
    private MoviesDbAdapter mDbAdapter;
    private boolean firstLoad;
    private OnFragmentInteractionListener mListener;
    private List<Movie> movieList = new ArrayList<>();
    public FavFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavFragment newInstance(String param1, String param2) {
        FavFragment fragment = new FavFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fav, container, false);

        moviesListView = (ListView) view.findViewById(R.id.movies_list_view);
        //when we click an individual item in the listview
        movieArrayAdapter = new MoviesArrayAdapter(getContext(), movieList);
        moviesListView.setAdapter(movieArrayAdapter);

        moviesListView.setDivider(null);
        mDbAdapter = new MoviesDbAdapter(getContext());
        mDbAdapter.open();



        moviesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> thisparent, View view, final int masterListPosition, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.MyDialogTheme);
                ListView modeListView = new ListView(getContext());
                String[] modes = new String[] {"Remove from favorites" };
                Movie mymovie = (Movie) thisparent.getItemAtPosition(masterListPosition);
                final String thismovieid = mymovie.getId();
                ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(getContext(),
                        android.R.layout.simple_list_item_1, android.R.id.text1, modes);
                modeListView.setAdapter(modeAdapter);
                builder.setView(modeListView);
                final Dialog dialog = builder.create();
                dialog.show();
                modeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        //edit movie
                        if (position == 0) {
                            mDbAdapter.deleteMovieById(thismovieid);
                            movieArrayAdapter.notifyDataSetChanged(); // rebind to ListView
                            moviesListView.smoothScrollToPosition(0); // scroll to top
                            populateFragment();
                        }
                        dialog.dismiss();
                    }
                });
            }
        });

        populateFragment();
        return view;
    }

    private void populateFragment(){
        movieList.clear();
        Cursor cursor = mDbAdapter.fetchAllMovies();

        try{
            if (cursor!=null && cursor.getCount() != 0){
                String mvid = cursor.getString(0);
                String mvrelease = cursor.getString(1);
                String mvvote = cursor.getString(2);
                String mvgenre = cursor.getString(3);
                String mvsynopsis = cursor.getString(4);
                String mvtitle = cursor.getString(5);
                String mvimage = cursor.getString(6);
                addMovietoList(mvid, mvgenre, mvvote, mvrelease, mvtitle, mvsynopsis, mvimage);
            }
            while (cursor.moveToNext()) {
                String mvid = cursor.getString(0);
                String mvrelease = cursor.getString(1);
                String mvvote = cursor.getString(2);
                String mvgenre = cursor.getString(3);
                String mvsynopsis = cursor.getString(4);
                String mvtitle = cursor.getString(5);
                String mvimage = cursor.getString(6);
                addMovietoList(mvid, mvgenre, mvvote, mvrelease, mvtitle, mvsynopsis, mvimage);
            }
        }
        finally {
            cursor.close();
        }
    }



    private void addMovietoList(String movie_id, String genre_ids, String vote_average,
                                String release_date, String title, String synopsis, String poster_path){

        movieList.add(new Movie(
                movie_id,
                genre_ids, // genre
                vote_average, // average votes
                release_date, // release date
                title,  // movie title
                synopsis, // movie synopsis
                poster_path // poster path
        ));

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onListFragmentInteraction(DummyContent.DummyItem item);

        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
