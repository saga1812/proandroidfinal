package edu.uchicago.saga.moviemill;
import java.text.NumberFormat;

public class Movie {
    public final String releaseDate;
    public final String voteAvg;
    public final String genreIDs;
    public final String synopsis;
    public final String title;
    public final String posterURL;
    public final String movieID;
    public String value;

    public Movie(String movieID, String genreIDs, String voteAvg, String releaseDate, String title,
                 String synopsis, String posterURL){
        NumberFormat numberFormat = NumberFormat.getInstance();
        this.movieID = movieID;
        this.title = title;
        this.synopsis = synopsis;
        this.releaseDate = releaseDate;
        this.posterURL =  posterURL;
        this.genreIDs = genreIDs;
        this.voteAvg = voteAvg ;
    }

    public String getTitle() {
        return this.title;
    }
    public String getId() {
        return this.movieID;
    }
    public  String getGenre() {return this.genreIDs;}
    public String getReleaseDate() { return this.releaseDate; }
    public String getSynopsis() { return this.synopsis; }
    public String getPosterURL() { return this.posterURL; }
    public String getVoteAvg() {
        return this.voteAvg;
    }




    public String toString(){
        return movieID;
    }


}
