package edu.uchicago.saga.moviemill;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import edu.uchicago.saga.moviemill.dummy.DummyContent;
import edu.uchicago.saga.moviemill.dummy.DummyContent.DummyItem;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */

// IMPORTANT NOTE
// This fragment allows you to search any movie by its title
// Then you can click on the search results and add the movie to your favorites list
// You can also remove a movie from your favorites list by clicking on the same.

public class MovieFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    private List<Movie> movieList = new ArrayList<>();

    // ArrayAdapter for binding Movie objects to a ListView
    private MoviesArrayAdapter movieArrayAdapter;
    private MoviesDbAdapter mDbAdapter;
    private ListView movieListView; // displays movie info
    private  EditText movieEditText;
    private FloatingActionButton fab;
    private  String mvid;

    public MovieFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static MovieFragment newInstance(int columnCount) {
        MovieFragment fragment = new MovieFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        movieEditText  = (EditText) view.findViewById(R.id.movieEditText);
        // Set the adapter
        // create ArrayAdapter to bind movieList to the movieListView
        movieListView = (ListView) view.findViewById(R.id.movieListView);
        movieArrayAdapter = new MoviesArrayAdapter(getContext(), movieList);
        movieListView.setAdapter(movieArrayAdapter);
        mDbAdapter = new MoviesDbAdapter(getContext());
        mDbAdapter.open();


        movieListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> thisparent, View view, final int masterListPosition, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.MyDialogTheme);
                ListView modeListView = new ListView(getContext());
                String[] modes = new String[] { "Add movie to favorites"};
                Movie mymovie = (Movie) thisparent.getItemAtPosition(masterListPosition);
                final String mymovieid = mymovie.getId();
                final String mymoviegenre = mymovie.getGenre();
                final String mymovietitle = mymovie.getTitle();
                final String mymoviesynopsis = mymovie.getSynopsis();
                final String mymovierelease = mymovie.getReleaseDate();
                final String mymovieimage = mymovie.getPosterURL();
                final String mymovievote = mymovie.getVoteAvg();


                ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(getContext(),
                        android.R.layout.simple_list_item_1, android.R.id.text1, modes);
                modeListView.setAdapter(modeAdapter);
                builder.setView(modeListView);
                final Dialog dialog = builder.create();
                dialog.show();
                modeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        //edit movie
                        if (position == 0) {

                            // Only add movie to database if it is not already present
                            int flag = 0;
                            Cursor cursor = mDbAdapter.fetchAllMovies();
                            if(cursor!=null && cursor.getCount() != 0) {
                                mvid = cursor.getString(0);
                                if (mvid.equals(mymovieid)) {
                                    flag = 1;
                                }
                            }
                            while (cursor.moveToNext()) {
                                mvid = cursor.getString(0);
                                if (mvid.equals(mymovieid)){
                                    flag=1;
                                    break;
                                }
                            }
                            if (flag==0){
                                mDbAdapter.createMovie(mymovieid, mymovierelease, mymovievote,
                                        mymoviegenre, mymoviesynopsis, mymovietitle, mymovieimage);
                            }

                        }
                        dialog.dismiss();
                    }
                });
            }
        });
        movieEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER){
                    fireUrl(fab);
                    return true;
                }
                return false;
            }
        });



        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fireUrl(view);
            }
        });


        return view;
    }


    private void fireUrl(View view) {
        String moviename= movieEditText.getText().toString();
        URL url = createURL(moviename);

        // hide keyboard and initiate a GetMovieTask to download
        // movie data from themoviedb.org in a separate thread
        if (url != null) {
            dismissKeyboard(movieEditText);
            GetMovieTask getLocalMovieTask = new GetMovieTask();
            getLocalMovieTask.execute(url);
        }
        else {

            //this may need to be in a catch block
            Snackbar.make(view, R.string.invalid_url, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        }
    }

    // programmatically dismiss keyboard when user touches FAB
    private void dismissKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    // create themoviedb.org web service URL using movie title
    private URL createURL(String title) {
        String apiKey = getString(R.string.api_key);
        String baseUrl = getString(R.string.web_service_url);

        try {
            // create URL for specified city movie api_key=927e12a4dacb5b2198746eaa9c99e656&amp;query=
            String urlString = baseUrl + getString(R.string.api_key) + "&"+ getString(R.string.query)
                    +URLEncoder.encode(title, "UTF-8");
            return new URL(urlString);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null; // URL was malformed
    }

    private class GetMovieTask
            extends AsyncTask<URL, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(URL... params) {


            JSONObject object = null;

            try {
                object = Util.getJSON(params[0].toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //System.out.println("object is "+ object);
            //Toast.makeText(getContext(), object.toString(), Toast.LENGTH_SHORT).show();
            return object;
        }

        // process JSON response and update ListView
        @Override
        protected void onPostExecute(JSONObject movie) {
            if (movie == null) {
                Toast.makeText(getContext(), "API did not reply", Toast.LENGTH_SHORT).show();
                return;
            }
            convertJSONtoArrayList(movie); // repopulate movieList
            movieArrayAdapter.notifyDataSetChanged(); // rebind to ListView
            movieListView.smoothScrollToPosition(0); // scroll to top
        }
    }


    // create Movie objects from JSONObject containing the reviews
    private void convertJSONtoArrayList(JSONObject reviews) {
        movieList.clear(); // clear old movie data

        try {
            // get review's "results" JSONArray
            JSONArray results = reviews.getJSONArray("results");

            // convert each element of list to a Movie object
            for (int i = 0; i < results.length(); ++i) {
                JSONObject movie = results.getJSONObject(i); // get one movie's data

                // get the movie's average votes ("vote_average") JSONObject
                double vote_average = movie.getDouble("vote_average");
                String converted_voteavg =  Double.toString(vote_average);
                // get movie's "genre"
                JSONArray array =
                        movie.getJSONArray("genre_ids");

                // Create an int array to accomodate the numbers.
                int[] genre_ids = new int[array.length()];

                Long movie_id =  movie.getLong("id");
                String converted_mvid = Long.toString(movie_id);

                // Extract numbers from JSON array.
                for (int j = 0; j < array.length(); j++) {
                    genre_ids[j] = array.getInt(j);
                }
                String converted_genre = genreIDs_to_genre_string(genre_ids);

                String release_date = movie.getString("release_date");

                String synopsis = movie.getString("overview");

                String title = movie.getString("original_title");

                String poster_path = "https://image.tmdb.org/t/p/w92" +
                        movie.getString("poster_path");

                // add new movie object to movieList
                movieList.add(new Movie(
                        converted_mvid,
                        converted_genre, // genre
                        converted_voteavg, // average votes
                        release_date, // release date
                        title,  // movie title
                        synopsis, // movie synopsis
                        poster_path // poster path
                        ));
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
    // The array for genre that we receive from the respose json object is an array of genre IDs
    // that are basically integers. We find the corresponding strings for these genre ids and make
    // a string for genre.
    public String genreIDs_to_genre_string(int[] genreIDarray) {
        String genreStringlist = "";
        String value = null;
        for (int i:genreIDarray){
            switch (i) {
                case 28:
                    value = "Action";
                    break;
                case 12:
                    value = "Adventure";
                    break;
                case 16:
                    value = "Animation";
                    break;
                case 35:
                    value = "Comedy";
                    break;
                case 80:
                    value = "Crime";
                    break;
                case 99:
                    value = "Documentary";
                    break;
                case 18:
                    value = "Drama";
                    break;
                case 10751:
                    value = "Family";
                    break;
                case 14:
                    value = "Fantasy";
                    break;
                case 36:
                    value = "History";
                    break;
                case 27:
                    value = "Horror";
                    break;
                case 10402:
                    value = "Music";
                    break;
                case 9648:
                    value = "Mystery";
                    break;
                case 10749:
                    value = "Romance";
                    break;
                case 878:
                    value = "Science Fiction";
                    break;
                case 10770:
                    value = "TV Movie";
                    break;
                case 53:
                    value = "Thriller";
                    break;
                case 10752:
                    value = "War";
                    break;
                case 37:
                    value = "Western";
                    break;
            }
            genreStringlist= genreStringlist + value + ", ";
        }
        if (genreStringlist != null && genreStringlist.length() > 0) {
            genreStringlist = genreStringlist.substring(0, genreStringlist.length() - 2);
        }
        return genreStringlist;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyContent.DummyItem item);

        void onFragmentInteraction(String message);
    }
}
