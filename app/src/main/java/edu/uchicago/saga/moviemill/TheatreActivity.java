// Referenced setting up directions between two markers from
// http://wptrafficanalyzer.in/blog/drawing-driving-route-directions-between-two-locations-using-google-directions-in-google-map-android-api-v2/


package edu.uchicago.saga.moviemill;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.UiSettings;

public class TheatreActivity extends AppCompatActivity implements OnMapReadyCallback {
    private List<Theatre> theatreList = new ArrayList<>();

    private MapView mapView;
    private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";
    private GoogleMap map;
    private LatLng currentPosition;
    private Polyline polyline;

    // ArrayAdapter for binding Weather objects to a ListView
    private TheatreArrayAdapter theatreArrayAdapter;
    private ListView theatreListView; // displays weather info
    private EditText locationEditText;
    private FloatingActionButton fab;
    private List<String> coordinates = new ArrayList<>();
    public Marker addMarker;
    private UiSettings mUiSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theatre);

        // create ArrayAdapter to bind theatreList to the theatreListView
        theatreListView = (ListView) findViewById(R.id.theatreListView);
        theatreArrayAdapter = new TheatreArrayAdapter(this, theatreList);
        theatreListView.setAdapter(theatreArrayAdapter);
        locationEditText = findViewById(R.id.locationEditText);
        locationEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    fireUrl(fab);
                    return true;
                }
                return false;
            }
        });


        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fireUrl(view);

            }
        });

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }
        mapView = (MapView) findViewById(R.id.theatreMapView);
        mapView.onCreate(mapViewBundle);

        mapView.getMapAsync(this);

        theatreListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int masterListPosition, long id) {

                Theatre theatre = (Theatre) parent.getItemAtPosition(masterListPosition);
                String latitude = theatre.getLatitude();
                String longitude = theatre.getLongitude();
                String name = theatre.getTheatreName();
                LatLng theatrePosition = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                if(addMarker!=null){
                    addMarker.remove();
                }
                addMarker = map.addMarker(new MarkerOptions().position(theatrePosition).title("Destination").snippet(name));

                // Getting URL to the Google Directions API
                URL url = getDirectionsUrl(currentPosition, theatrePosition);
                if(polyline!=null){
                    polyline.remove();
                }
                DownloadTask downloadTask = new DownloadTask();

                // Start downloading json data from Google Directions API
                downloadTask.execute(url);
            }
        });
    }


    private URL getDirectionsUrl(LatLng origin,LatLng dest){
        try{
            // Origin
            String str_origin = getString(R.string.origin)+origin.latitude+","+origin.longitude;
            // Destination
            String str_dest = getString(R.string.destination)+dest.latitude+","+dest.longitude;
            String sensor = getString(R.string.sensor);
            String parameters = str_origin+"&"+str_dest+"&"+sensor;
            String url = getString(R.string.direction_query_url) + parameters;
            return new URL(url);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null; // URL was malformed
    }

    private class DownloadTask extends AsyncTask<URL, Void, JSONObject>{

        @Override
        protected JSONObject doInBackground(URL... params) {
            JSONObject object = null;
            try {
                object = Util.getJSON(params[0].toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }

        @Override
        protected void onPostExecute(JSONObject direction) {
            if (direction == null) {
                Toast.makeText(TheatreActivity.this, "API did not reply", Toast.LENGTH_SHORT).show();
                return;
            }
            ParserTask parserTask = new ParserTask();
            // Invokes the thread for parsing the JSON data
            parserTask.execute(direction);
        }
    }

    /** A class to parse cthe Google Places in JSON format */
    private class ParserTask extends AsyncTask<JSONObject, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(JSONObject... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = jsonData[0];
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            if(lineOptions!=null){
                polyline = map.addPolyline(lineOptions);
                zoomRoute(map, points);
            }
        }
    }

    public void zoomRoute(GoogleMap googleMap, List<LatLng> lstLatLngRoute) {

        if (googleMap == null || lstLatLngRoute == null || lstLatLngRoute.isEmpty()) return;

        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        for (LatLng latLngPoint : lstLatLngRoute)
            boundsBuilder.include(latLngPoint);

        int routePadding = 100;
        LatLngBounds latLngBounds = boundsBuilder.build();

        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));
    }

    public class DirectionsJSONParser {

        /**
         * Receives a JSONObject and returns a list of lists containing latitude and longitude
         */
        public List<List<HashMap<String, String>>> parse(JSONObject jObject) {

            List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
            JSONArray jRoutes = null;
            JSONArray jLegs = null;
            JSONArray jSteps = null;

            try {

                jRoutes = jObject.getJSONArray("routes");

                /** Traversing all routes */
                for (int i = 0; i < jRoutes.length(); i++) {
                    jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                    List path = new ArrayList<HashMap<String, String>>();

                    /** Traversing all legs */
                    for (int j = 0; j < jLegs.length(); j++) {
                        jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                        /** Traversing all steps */
                        for (int k = 0; k < jSteps.length(); k++) {
                            String polyline = "";
                            polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                            List<LatLng> list = decodePoly(polyline);

                            /** Traversing all points */
                            for (int l = 0; l < list.size(); l++) {
                                HashMap<String, String> hm = new HashMap<String, String>();
                                hm.put("lat", Double.toString(((LatLng) list.get(l)).latitude));
                                hm.put("lng", Double.toString(((LatLng) list.get(l)).longitude));
                                path.add(hm);
                            }
                        }
                        routes.add(path);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }
            return routes;
        }

        private List<LatLng> decodePoly(String encoded) {

            List<LatLng> poly = new ArrayList<LatLng>();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;

            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng p = new LatLng((((double) lat / 1E5)),
                        (((double) lng / 1E5)));
                poly.add(p);
            }

            return poly;
        }

    }



    private void fireUrl(View view) {
        URL url = createAddressSearchURL(locationEditText.getText().toString());

        // hide keyboard and initiate a GetWeatherTask to download
        // weather data from OpenWeatherMap.org in a separate thread
        if (url != null) {
            dismissKeyboard(locationEditText);
            GetCoordinatesTask getLocalCoordinatesTask = new GetCoordinatesTask();
            getLocalCoordinatesTask.execute(url);
        } else {
            Snackbar.make(view, R.string.invalid_url, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }

    }

    // programmatically dismiss keyboard when user touches FAB
    private void dismissKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    // create google geocoding web service URL using address entered in locationEditText
    private URL createAddressSearchURL(String address) {
        try {
            // create URL for specified address and imperial units (Fahrenheit)
            String urlString = getString(R.string.geocoding_url) + URLEncoder.encode(address, "UTF-8") +
                    "&key=" + getString(R.string.google_api_key);
            return new URL(urlString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null; // URL was malformed
    }

    private URL createTheatresSearchURL(String lat, String lon) {
        try {
            // create URL for specified address and imperial units (Fahrenheit)
            String urlString = getString(R.string.theatre_search_prefix) +
                    lat + "," + lon + getString(R.string.theatre_search_url) + getString(R.string.google_api_key);
            return new URL(urlString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null; // URL was malformed
    }


    private class GetCoordinatesTask
            extends AsyncTask<URL, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(URL... params) {
            JSONObject object = null;
            try {
                object = Util.getJSON(params[0].toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }

        // process JSON response and update ListView
        @Override
        protected void onPostExecute(JSONObject location) {
            if (location == null) {
                Toast.makeText(TheatreActivity.this, "API did not reply", Toast.LENGTH_SHORT).show();
                return;
            }

            convertJSONtoArrayList(location);
            //  theatreArrayAdapter.notifyDataSetChanged(); // rebind to ListView
            //  theatreListView.smoothScrollToPosition(0); // scroll to top
            String currentLatitude = coordinates.get(0);
            String currentLongitude = coordinates.get(1);
            URL theatresURL = createTheatresSearchURL(currentLatitude, currentLongitude);
            if (theatresURL != null) {
                dismissKeyboard(locationEditText);
                GetTheatresTask getLocalTheatresTask = new GetTheatresTask();
                getLocalTheatresTask.execute(theatresURL);
            }

        }

    }


    // create Weather objects from JSONObject containing the forecast
    private void convertJSONtoArrayList(JSONObject locationresults) {

        try {
            // get address search's "results" JSONArray
            JSONArray results = locationresults.getJSONArray("results");

            for (int i = 0; i < results.length(); ++i) {
                JSONObject address = results.getJSONObject(0); // get address

                // get the movie's average votes ("vote_average") JSONObject
                JSONObject geometry = address.getJSONObject("geometry");
                JSONObject location = geometry.getJSONObject("location");
                Double latitude = location.getDouble("lat");
                String converted_latitude = Double.toString(latitude);
                Double longitude = location.getDouble("lng");
                String converted_longitude = Double.toString(longitude);
                coordinates.clear();
                coordinates.add(converted_latitude);
                coordinates.add(converted_longitude);
                currentPosition = new LatLng(Double.parseDouble(coordinates.get(0)), Double.parseDouble(coordinates.get(1)));
                map.addMarker(new MarkerOptions().position(currentPosition).title("Start").snippet("Your Position"));
                moveToCurrentLocation(currentPosition);
            }

        } catch (JSONException e1) {
            e1.printStackTrace();
        }

    }

    private void moveToCurrentLocation(LatLng currentLocation)
    {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation,15));
        // Zoom in, animating the camera.
        map.animateCamera(CameraUpdateFactory.zoomIn());
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);


    }

    private class GetTheatresTask
            extends AsyncTask<URL, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(URL... params) {
            JSONObject object = null;
            try {
                object = Util.getJSON(params[0].toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return object;
        }

        // process JSON response and update ListView
        @Override
        protected void onPostExecute(JSONObject theatres) {
            if (theatres == null) {
                Toast.makeText(TheatreActivity.this, "API did not reply", Toast.LENGTH_SHORT).show();
                return;
            }

            convertJSONtoTheatreList(theatres); // populate theatresList
            theatreArrayAdapter.notifyDataSetChanged(); // rebind to ListView
            theatreListView.smoothScrollToPosition(0); // scroll to top

        }

    }


    // create Weather objects from JSONObject containing the forecast
    private void convertJSONtoTheatreList(JSONObject theatreResults) {
        theatreList.clear();
        try {
            // get address search's "results" JSONArray
            JSONArray results = theatreResults.getJSONArray("results");

            // convert each element of list to a Theatre object
            for (int i = 0; i < results.length(); ++i) {
                JSONObject theatre = results.getJSONObject(i); // get one movie's data

                // get the theatre's ratings
                String converted_rating = "NA";
                if (theatre.has("rating")) {
                    double rating = theatre.getDouble("rating");
                    converted_rating = Double.toString(rating);
                }
                // get theatre's name
                String name = theatre.getString("name");

                // get theatre's open hours
                String open = "NA";
                if (theatre.has("opening_hours")) {
                    JSONObject openHours = theatre.getJSONObject("opening_hours");
                    Boolean openNow = openHours.getBoolean("open_now");
                    if (openNow == true) {
                        open = "Yes";
                    } else {
                        open = "No";
                    }
                }

                // get theatre's latitude
                JSONObject geometry = theatre.getJSONObject("geometry");
                JSONObject location = geometry.getJSONObject("location");
                Double latitude = location.getDouble("lat");
                String converted_latitude = Double.toString(latitude);
                Double longitude = location.getDouble("lng");
                String converted_longitude = Double.toString(longitude);
                // get theatre's icon path
                String icon_path = theatre.getString("icon");
                String distance = getDistanceFromLatLonInKm(Double.parseDouble(coordinates.get(0)),
                        Double.parseDouble(coordinates.get(1)), latitude, longitude);

                // add new movie object to movieList
                theatreList.add(new Theatre(
                        name,              // theatre name
                        converted_rating, // average votes
                        open,               // open now?
                        converted_latitude,
                        converted_longitude,
                        distance,
                        icon_path // icon path
                ));

                System.out.println(theatreList.toString());

            }

        } catch (JSONException e1) {
            e1.printStackTrace();
        }

    }

    public String getDistanceFromLatLonInKm(double lat1, double lon1, double lat2, double lon2) {
        int R = 6371; // Radius of the earth in km
        double dLat = deg2rad(lat2-lat1);  // deg2rad below
        double dLon = deg2rad(lon2-lon1);
        double a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                                Math.sin(dLon/2) * Math.sin(dLon/2)
                ;
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c; // Distance in km
        return Double.toString(d).substring(0,3)+" km";
    }

    public double deg2rad(double deg) {
        return deg * (Math.PI/180);
    }
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onMapReady(GoogleMap gmap) {
          map = gmap;
          map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
          mUiSettings = map.getUiSettings();
          mUiSettings.setZoomGesturesEnabled(true);
          mUiSettings.setZoomControlsEnabled(true);
    }

    @Override
    protected void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

}


