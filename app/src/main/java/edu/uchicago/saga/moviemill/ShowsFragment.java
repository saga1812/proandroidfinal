package edu.uchicago.saga.moviemill;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ShowsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ShowsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */


// Displays a list of all movies currently playing in theatres

public class ShowsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Button theatreButon;

    private List<Movie> movieList = new ArrayList<>();

    // ArrayAdapter for binding Movie objects to a ListView
    private MoviesArrayAdapter movieArrayAdapter;
    private MoviesDbAdapter mDbAdapter;
    private ListView moviesListView; // displays movie info


    private OnFragmentInteractionListener mListener;

    public ShowsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ShowsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ShowsFragment newInstance(String param1, String param2) {
        ShowsFragment fragment = new ShowsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shows, container, false);
        theatreButon = view.findViewById(R.id.theatresButton);
        moviesListView = (ListView) view.findViewById(R.id.movies_list_view);
        movieArrayAdapter = new MoviesArrayAdapter(getContext(), movieList);
        moviesListView.setAdapter(movieArrayAdapter);
        mDbAdapter = new MoviesDbAdapter(getContext());
        mDbAdapter.open();
        URL url = createURL();
        if (url != null) {
            GetMovieTask getLocalMovieTask = new GetMovieTask();
            getLocalMovieTask.execute(url);
        }
        else {
            //this may need to be in a catch block
            Snackbar.make(view, R.string.invalid_url, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        }

        theatreButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TheatreActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    // create themoviedb.org web service URL using movie title
    private URL createURL() {

        try {
            // create URL for specified movie api_key=927e12a4dacb5b2198746eaa9c99e656&amp;query=
            String urlString = getString(R.string.now_playing_query) + getString(R.string.api_key) + "&" + getString(R.string.trailing_querystring) ;
            return new URL(urlString);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null; // URL was malformed
    }

    private class GetMovieTask
            extends AsyncTask<URL, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(URL... params) {


            JSONObject object = null;

            try {
                object = Util.getJSON(params[0].toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //System.out.println("object is "+ object);
            //Toast.makeText(getContext(), object.toString(), Toast.LENGTH_SHORT).show();
            return object;
        }

        // process JSON response and update ListView
        @Override
        protected void onPostExecute(JSONObject movie) {
            if (movie == null) {
                Toast.makeText(getContext(), "API did not reply", Toast.LENGTH_SHORT).show();
                return;
            }
            convertJSONtoArrayList(movie); // repopulate movieList
            movieArrayAdapter.notifyDataSetChanged(); // rebind to ListView
            moviesListView.smoothScrollToPosition(0); // scroll to top
        }
    }


    // create Movie objects from JSONObject containing the reviews
    private void convertJSONtoArrayList(JSONObject reviews) {
        movieList.clear(); // clear old movie data

        try {
            // get review's "results" JSONArray
            JSONArray results = reviews.getJSONArray("results");

            // convert each element of list to a Movie object
            for (int i = 0; i < results.length(); ++i) {
                JSONObject movie = results.getJSONObject(i); // get one movie's data

                // get the movie's average votes ("vote_average") JSONObject
                double vote_average = movie.getDouble("vote_average");
                String converted_voteavg =  Double.toString(vote_average);
                // get movie's "genre"
                JSONArray array =
                        movie.getJSONArray("genre_ids");

                // Create an int array to accomodate the numbers.
                int[] genre_ids = new int[array.length()];

                Long movie_id =  movie.getLong("id");
                String converted_mvid = Long.toString(movie_id);

                // Extract numbers from JSON array.
                for (int j = 0; j < array.length(); j++) {
                    genre_ids[j] = array.getInt(j);
                }
                String converted_genre = genreIDs_to_genre_string(genre_ids);

                String release_date = movie.getString("release_date");

                String synopsis = movie.getString("overview");

                String title = movie.getString("original_title");

                String poster_path = "https://image.tmdb.org/t/p/w92" +
                        movie.getString("poster_path");

                // add new movie object to movieList
                movieList.add(new Movie(
                        converted_mvid,
                        converted_genre, // genre
                        converted_voteavg, // average votes
                        release_date, // release date
                        title,  // movie title
                        synopsis, // movie synopsis
                        poster_path // poster path
                ));
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
    // The array for genre that we receive from the respose json object is an array of genre IDs
    // that are basically integers. We find the corresponding strings for these genre ids and make
    // a string for genre.
    public String genreIDs_to_genre_string(int[] genreIDarray) {
        String genreStringlist = "";
        String value = null;
        for (int i:genreIDarray){
            switch (i) {
                case 28:
                    value = "Action";
                    break;
                case 12:
                    value = "Adventure";
                    break;
                case 16:
                    value = "Animation";
                    break;
                case 35:
                    value = "Comedy";
                    break;
                case 80:
                    value = "Crime";
                    break;
                case 99:
                    value = "Documentary";
                    break;
                case 18:
                    value = "Drama";
                    break;
                case 10751:
                    value = "Family";
                    break;
                case 14:
                    value = "Fantasy";
                    break;
                case 36:
                    value = "History";
                    break;
                case 27:
                    value = "Horror";
                    break;
                case 10402:
                    value = "Music";
                    break;
                case 9648:
                    value = "Mystery";
                    break;
                case 10749:
                    value = "Romance";
                    break;
                case 878:
                    value = "Science Fiction";
                    break;
                case 10770:
                    value = "TV Movie";
                    break;
                case 53:
                    value = "Thriller";
                    break;
                case 10752:
                    value = "War";
                    break;
                case 37:
                    value = "Western";
                    break;
            }
            genreStringlist= genreStringlist + value + ", ";
        }
        if (genreStringlist != null && genreStringlist.length() > 0) {
            genreStringlist = genreStringlist.substring(0, genreStringlist.length() - 2);
        }
        return genreStringlist;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
